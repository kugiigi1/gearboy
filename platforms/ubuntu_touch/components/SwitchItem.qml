import QtQuick 2.9
import Ubuntu.Components 1.3

ListItem {
    id: switchItem
    
    property alias checked: toggle.checked
    property alias titleText: listItemLayout.title
    property alias subText: listItemLayout.subtitle
    
    width: parent.width
    
    ListItemLayout {
        id: listItemLayout

        Switch {
            id: toggle
            SlotsLayout.position: SlotsLayout.Trailing
        }
    }

    onClicked: {
        checked = !checked
    }
}
